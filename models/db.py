# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# AppConfig configuration made easy. Look inside private/appconfig.ini
# Auth is for authenticaiton and access control
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig
from gluon.tools import Auth

# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

if request.global_settings.web2py_version < "2.15.5":
    raise HTTP(500, "Requires web2py 2.15.5 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
configuration = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(configuration.get('db.uri'),
             pool_size=configuration.get('db.pool_size'),
             migrate_enabled=configuration.get('db.migrate'),
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = []
if request.is_local and not configuration.get('app.production'):
    response.generic_patterns.append('*')

# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = 'bootstrap4_inline'
response.form_label_separator = ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db, host_names=configuration.get('host.names'))

# -------------------------------------------------------------------------
# create all tables needed by auth, maybe add a list of extra fields
# -------------------------------------------------------------------------
auth.settings.extra_fields['auth_user'] = []
auth.define_tables(username=False, signature=False)
auth.settings.actions_disabled.append('register')
auth.settings.actions_disabled.append('request_reset_password')

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else configuration.get('smtp.server')
mail.settings.sender = configuration.get('smtp.sender')
mail.settings.login = configuration.get('smtp.login')
mail.settings.tls = configuration.get('smtp.tls') or False
mail.settings.ssl = configuration.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------
# read more at http://dev.w3.org/html5/markup/meta.name.html
# -------------------------------------------------------------------------
response.meta.author = configuration.get('app.author')
response.meta.description = configuration.get('app.description')
response.meta.keywords = configuration.get('app.keywords')
response.meta.generator = configuration.get('app.generator')

# -------------------------------------------------------------------------
# your http://google.com/analytics id
# -------------------------------------------------------------------------
response.google_analytics_id = configuration.get('google.analytics_id')

# -------------------------------------------------------------------------
# maybe use the scheduler
# -------------------------------------------------------------------------
if configuration.get('scheduler.enabled'):
    from gluon.scheduler import Scheduler
    scheduler = Scheduler(db, heartbeat=configure.get('heartbeat'))

# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
# auth.enable_record_versioning(db)

""" database class object creation (initialization) """
if request.env.web2py_runtime_gae:                  # if running on Google App Engine
    dbOBJECT = DAL('gae')                           # connect to Google BigTable
    session.connect(request, response, db=dbOBJECT) # and store sessions and tickets there
else:                                               # else use a normal relational database
    #Importante migrate=False, fake_migrate=False para evitar tablas corruptas ¡Remover en producción!
    dbOBJECT = DAL("sqlite://dbOBJECT.db")

def Quien_Carga():
    #Función para evitar error cuando un usuario no registrado visita la página
    if auth.user == None:
        return None
    else:
        return auth.user.email

dbOBJECT.define_table("Tipo_Evento",
    Field("Tipo", #Nombre del evento
        "string",
        default=None,
        requires=IS_NOT_EMPTY(),
        label='Tipo de Evento',
        required=True,
        length=100,
            ),
    Field("Descripcion", #Nombre del evento
        "string",
        default=None,
        requires=IS_NOT_EMPTY(),
        label='Descripción del evento',
        required=True,
        length=100,
            ),
    Field("Carga",
        default=Quien_Carga(),
        writable=False,
        readable=False),
    )


## TABLA EVENTOS
dbOBJECT.define_table("Eventos",
    Field("Nombre", #Nombre del evento
        "string",
        default=None,
        requires=IS_NOT_EMPTY(),
        label='Nombre del Evento',
        required=True,
        length=100,
            ),

    Field("Tipo_Evento",
        "reference Tipo_Evento",
        requires=IS_IN_DB(dbOBJECT,dbOBJECT.Tipo_Evento.id,"%(Tipo)s"),
        required=True,
        label="Tipo de Evento"),

    ## Responsable del evento
    Field("Responsable",
        "string",
        default=None,
        requires=IS_NOT_EMPTY(),
        label='Responsable del Evento',
        required=True,
        length=100),

    ## Fecha en la que se desarrolla el evento
    Field("Fecha",
        "date",
        requires=IS_DATE(format = T('%d/%m/%Y')),
        default=request.now.date(),
        required=True,
        label="Fecha"),

    ## Hora de inicio del evento
    Field("Hora_Inicial",
        "time", ##TODO: Colocar campo de requires (Formato HH:MM)
        default=None,
        label="Inicio"),
    ## Hora de final del evento
    Field("Hora_Final",
        "time", ##TODO: Colocar campo de requires (Formato HH:MM)
        default=None,
        label="Final"),

    #Datos de Carga auth_user.email
    Field("Carga",
        default=Quien_Carga(),
        writable=False,
        readable=False),
    )
