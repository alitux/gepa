# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

# ---- example index page ----
def index():
    lista_eventos = SQLFORM.smartgrid(dbOBJECT.Eventos)
    return dict(lista_eventos=lista_eventos)

# --- Función para cargar Evento
def carga_evento():
   formulario_evento=SQLFORM(dbOBJECT.Eventos)
   if formulario_evento.process().accepted:
       session.flash = '¡Carga de evento correcta!'
       redirect(URL('carga_evento'))
   elif formulario_evento.errors:
       session.flash = 'Verifique las entradas'
   else:
       session.flash = 'Por Favor completar formulario'
   return dict(formulario_evento=formulario_evento)

# --- Función para cargar Evento
def carga_tipo_evento():
   formulario_tipo_evento=SQLFORM(dbOBJECT.Tipo_Evento)
   if formulario_tipo_evento.process().accepted:
       session.flash = '¡Carga de tipo de evento correcta!'
       redirect(URL('carga_evento'))
   elif formulario_tipo_evento.errors:
       session.flash = 'Verifique las entradas'
   else:
       session.flash = 'Por Favor completar formulario'
   return dict(formulario_tipo_evento=formulario_tipo_evento)
